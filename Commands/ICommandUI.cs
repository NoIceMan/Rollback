﻿using System.Collections.Generic;

namespace TaskRollback.Commands
{
    internal interface ICommandUI
    {
        uint GetUint(string getMessage);

        void ShowMessage(params string[] strings);

        void ShowCollection<T>(IEnumerable<T> collection);

        void CloseApplication();
    }
}
