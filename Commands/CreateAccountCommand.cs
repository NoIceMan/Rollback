﻿using TaskRollback.Accounts;

namespace TaskRollback.Commands
{
    internal class CreateAccountCommand : Command, IRollbacked
    {
        private uint idAccount;
        private uint valueAccount;

        internal sealed override void Do(AccountsBank accountsBank, ICommandUI commandUI)
        {
            do
                idAccount = commandUI.GetUint("Enter ID account");
            while (accountsBank.IsExistAccount(idAccount));

            valueAccount = commandUI.GetUint("Enter value account");

            var account = Account.Create()
                .SetId(idAccount)
                .SetValue(valueAccount)
                .Build();

            if (accountsBank.TryAddAccount(account))
                accountsBank.Make(this as IRollbacked);
        }

        public void DoBack(AccountsBank accountsBank, ICommandUI commandUI)
        {
            var extractedAccount = accountsBank.ExtractAccountById(idAccount);
            if (extractedAccount != null)
                commandUI.ShowMessage("Deleted account with id =", extractedAccount.Id.ToString());
        }
    }
}
