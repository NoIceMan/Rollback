﻿using TaskRollback.Accounts;

namespace TaskRollback.Commands
{
    internal class CloseAccountCommand : Command, IRollbacked
    {
        private uint idAccount;
        private uint valueAccount;

        internal sealed override void Do(AccountsBank accountsBank, ICommandUI commandUI)
        {
            idAccount = commandUI.GetUint("Enter ID account for close");
            var extractedAccount = accountsBank.ExtractAccountById(idAccount);

            if (extractedAccount == null)
            {
                commandUI.ShowMessage("Account with ID", idAccount.ToString(), "not exist");
            }
            else
            {
                valueAccount = extractedAccount.Value;
                accountsBank.Make(this as IRollbacked);
                commandUI.ShowMessage("Deleted account with id =", extractedAccount.Id.ToString());
            }
        }

        public void DoBack(AccountsBank accountsBank, ICommandUI commandUI)
        {
            if (accountsBank.IsExistAccount(idAccount))
            {
                commandUI.ShowMessage("Сannot be undone tranaction: close account with ID", idAccount.ToString());
            }
            else
            {
                var account = Account.Create()
                    .SetId(idAccount)
                    .SetValue(valueAccount)
                    .Build();

                accountsBank.TryAddAccount(account);
                commandUI.ShowMessage("Created account with id =", idAccount.ToString(), "and value =", valueAccount.ToString());
            }
        }
    }
}
