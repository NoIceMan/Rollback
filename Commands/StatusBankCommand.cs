﻿using TaskRollback.Accounts;

namespace TaskRollback.Commands
{
    internal class StatusBankCommand : Command
    {
        internal sealed override void Do(AccountsBank accountsBank, ICommandUI commandUI)
        {
            commandUI.ShowCollection(accountsBank.GetStateAccounts());
        }
    }
}
