﻿using TaskRollback.Accounts;

namespace TaskRollback.Commands
{
    internal class UndoCommand : Command
    {
        internal sealed override void Do(AccountsBank accountsBank, ICommandUI commandUI)
        {
            var lastTranaction = accountsBank.ExtractLastTranaction();
            if (lastTranaction != null)
                lastTranaction.DoBack(accountsBank, commandUI);
        }
    }
}
